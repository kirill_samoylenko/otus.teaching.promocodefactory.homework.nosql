using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();

            ConventionRegistry.Register("Camel Case", new ConventionPack { new CamelCaseElementNameConvention() }, _ => true);

            //services.AddSingleton<IMongoClient>(s => new MongoClient(Configuration["MongoDbPromocodeFactoryAdministration:ConnectionString"]));
            //services.AddScoped(s => new DataContext(s.GetRequiredService<IMongoClient>(), Configuration["MongoDbPromocodeFactoryAdministration:PromocodeFactoryAdministration"]));

            var mongoClient = new MongoClient(Configuration["MongoDbPromocodeFactoryAdministration:ConnectionString"]);
            var database = mongoClient.GetDatabase(Configuration["MongoDbPromocodeFactoryAdministration:Database"]);
            var entityCollection = database.GetCollection<Employee>("Employee");
            var roleCollection = database.GetCollection<Role>("Role");

            services.AddSingleton<IMongoClient>(_ => mongoClient);
            services.AddSingleton(serviceProvider => database);
            services.AddSingleton(serviceProvider => entityCollection);
            services.AddSingleton(serviceProvider => roleCollection);

            services.AddScoped(serviceProvider =>
                                serviceProvider.GetRequiredService<IMongoClient>()
                .StartSession());



            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Administration API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            dbInitializer.InitializeDb();
        }
    }
}