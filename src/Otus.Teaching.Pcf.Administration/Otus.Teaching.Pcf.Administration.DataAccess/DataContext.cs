﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class DataContext
    {
        private readonly IMongoDatabase _db;

        public DataContext(IMongoClient client, string dbName)
        {
            _db = client.GetDatabase(dbName);
        }

        public IMongoCollection<Role> Roles => _db.GetCollection<Role>("roles");

        public IMongoCollection<Employee> Employees => _db.GetCollection<Employee>("employees");
    }
}